<html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Fondamento&display=swap" rel="stylesheet">
    <!-- Bootswatch CSS -->
    <link rel="stylesheet" href="https://bootswatch.com/4/cosmo/bootstrap.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="/styles/styles2.css">
    <title>Your Birthstone Result</title>
</head>
<body>

    <div id="resultid" class="d-flex justify-content-center align-items-center vh-100 flex-column">

    <?php 

        $fullName = $_POST['fullName'];
        $month = $_POST['month'];

        if($fullName === '' && $month === ''){
            echo "Your details are required.";
            return;
        };

        if($month === 'January'){
            echo $fullName . " , Your Birth Stone is <b>GARNET</b> <br>";
            echo "Know More About Your Birthstone meaning <br>";
            echo "<a href=https://www.uncommongoods.com/blog/2018/birthstones-by-month-what-do-they-mean/ target=_blank>Here</a>";
            return;
        }else if($month === 'February'){
            echo $fullName . " , Your Birth Stone is <b>AMETHYST</b> <br>";
            echo "Know More About Your Birthstone meaning <br>";
            echo "<a href=https://www.uncommongoods.com/blog/2018/birthstones-by-month-what-do-they-mean/ target=_blank>Here</a>";
            return;
        }else if($month === 'March'){
            echo $fullName . " , Your Birth Stone is <b>AQUAMARINE</b> <br>";
            echo "Know More About Your Birthstone meaning <br>";
            echo "<a href=https://www.uncommongoods.com/blog/2018/birthstones-by-month-what-do-they-mean/ target=_blank>Here</a>";
            return;
        }else if($month === 'April'){
            echo $fullName . " , Your Birth Stone is <b>DIAMOND</b> <br>";
            echo "Know More About Your Birthstone meaning <br>";
            echo "<a href=https://www.uncommongoods.com/blog/2018/birthstones-by-month-what-do-they-mean/ target=_blank>Here</a>";
            return;
        }else if($month === 'May'){
            echo $fullName . " , Your Birth Stone is <b>EMERALD</b> <br>";
            echo "Know More About Your Birthstone meaning <br>";
            echo "<a href=https://www.uncommongoods.com/blog/2018/birthstones-by-month-what-do-they-mean/ target=_blank>Here</a>";
            return;
        }else if($month === 'June'){
            echo $fullName . " , Your Birth Stone is <b>PEARL</b> <br>";
            echo "Know More About Your Birthstone meaning <br>";
            echo "<a href=https://www.uncommongoods.com/blog/2018/birthstones-by-month-what-do-they-mean/ target=_blank>Here</a>";
            return;
        }else if($month === 'July'){
            echo $fullName . " , Your Birth Stone is <b>RUBY</b> <br>";
            echo "Know More About Your Birthstone meaning <br>";
            echo "<a href=https://www.uncommongoods.com/blog/2018/birthstones-by-month-what-do-they-mean/ target=_blank>Here</a>";
            return;
        }else if($month === 'August'){
            echo $fullName . " , Your Birth Stone is <b>PERIDOT</b> <br>";
            echo "Know More About Your Birthstone meaning <br>";
            echo "<a href=https://www.uncommongoods.com/blog/2018/birthstones-by-month-what-do-they-mean/ target=_blank>Here</a>";
            return;
        }else if($month === 'September'){
            echo $fullName . " , Your Birth Stone is <b>SAPPHIRE</b> <br>";
            echo "Know More About Your Birthstone meaning <br>";
            echo "<a href=https://www.uncommongoods.com/blog/2018/birthstones-by-month-what-do-they-mean/ target=_blank>Here</a>";
            return;
        }else if($month === 'October'){
            echo $fullName .  " , Your Birth Stone is <b>OPAL</b> <br>";
            echo "Know More About Your Birthstone meaning <br>";
            echo "<a href=https://www.uncommongoods.com/blog/2018/birthstones-by-month-what-do-they-mean/ target=_blank>Here</a>";
            return;
        }else if($month === 'November'){
            echo $fullName . " , Your Birth Stone is <b>TOPAZ</b> <br>";
            echo "Know More About Your Birthstone meaning <br>";
            echo "<a href=https://www.uncommongoods.com/blog/2018/birthstones-by-month-what-do-they-mean/ target=_blank>Here</a>";
            return;
        }else if($month === 'December'){
            echo $fullName . " , Your Birth Stone is <b>ZIRCON</b> <br>";
            echo "Know More About Your Birthstone meaning <br>";
            echo "<a href=https://www.uncommongoods.com/blog/2018/birthstones-by-month-what-do-they-mean/ target=_blank>Here</a>";
            return;
        }

    ?>
    </div>
</body>
</html>