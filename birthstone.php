<html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP&display=swap" rel="stylesheet">
    <!-- Bootswatch CSS -->
    <link rel="stylesheet" href="https://bootswatch.com/4/cosmo/bootstrap.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="styles/styles.css">
    <title>What's Your Birthstone?</title>
</head>
<body>

    <div class="d-flex justify-content-center align-items-center vh-100 flex-column">
        <h3><b>Your birth stone tells something about you and your personality.</b></h3>
        <h4><b><i>Know more about it today!</i></b></h4>
        <br>
        <br>
        <br>
        <hr>
        <form action="controllers/process_birthstone.php" method="POST">
                <div class="form-group">
                    <label for="fullName">What's Your Full Name?</label>
                    <input type="fullName" name="fullName" class="form-control">
                </div>
                <div class="form-group">
                    <label for="month">Your Month of Birth?</label>
                    <input type="month" name="month" class="form-control">
                </div>
                    <button type="submit" class="btn btn-info">Check Your BirthStone!</button>
        </form>
    </div>
</body>
</html>