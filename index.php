<html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://bootswatch.com/4/materia/bootstrap.css">
    <title>Age Checker</title>
</head>
<body>
    <div class="d-flex justify-content-center align-items-center flex-column vh-100">
        <h1>Age Checker</h1>

        <!-- METHOD = POST -->
        <form action="controllers/process_age_checker.php" method="POST">
            <div class="form-group">
                <label for="age">What's Your Age?</label>
                <!-- INPUTS MUST HAVE A NAME -->
                <input type="number" name="age" class="form-control">
            </div>
            <!-- BUTTON INSIDE THE FORM MUST BE TYPE SUBMIT -->
            <button type="submit" class="btn btn-success">Submit</button>
        </form>
    </div>
</body>
</html>