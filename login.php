<html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://bootswatch.com/4/materia/bootstrap.css">
    <title>Login Page</title>
</head>
<body>
    <div class="d-flex justify-content-center align-items-center vh-100 flex-column">
        <h1>Login Page</h1>
        <form action="controllers/process_login.php" method="POST">
        <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" name="email" class="form-control">
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" name="password" class="form-control">
            </div>
            <button type="submit" class="btn btn-info">Login</button>
        </form>
    </div>
</body>
</html>